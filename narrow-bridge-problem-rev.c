    /***********************************************************************
     * Name(s)  Daniel Koenig                                              *
     * Box(s):  C127                                                       *
     * Assignment name: Narrow Bridge Problem                              *
     * Assignment for 5/2/19                                               *
     ***********************************************************************/

    /* *********************************************************************
     * Academic honesty certification:                                     *
     *   Written/online sources used: StackOverflow, GeeksForGeeks         *
     *   Help obtained: Professor Walker                                   *
     *   My/our signature(s) below confirms that the above list of sources *
     *   is complete AND that I/we have not talked to anyone else          *
     *   (e.g., CSC 161 students) about the solution to this problem       *
     *                                                                     *
     *   Signature:                                                        *
     ***********************************************************************/

#include <sys/types.h>     /* for pid_t */
#include <unistd.h>        /* for fork */
#include <stdio.h>         /* for printf */
#include <sys/wait.h>      /* for waitpid */
#include <stdlib.h>        /* for exit */
#include <semaphore.h>       /* for semaphore processing */
#include <sys/mman.h>      /* for mmap */
#include <errno.h>	// for errno
#include <fcntl.h> //for o_ constants
#include <time.h> //for seeding srand

#define SHARED_MEM_SIZE 8 * sizeof(int)

#define OPEN_SEM_MODE 0644
#define NUM_WAGONS_TO_SPAWN 30

#define NORTH 1
#define SOUTH -1

#define MIN_TIME_TO_SPAWN_NEW_WAGON 1
#define MAX_TIME_TO_SPAWN_NEW_WAGON 4

#define MIN_WAGON_CROSSING_TIME 3
#define MAX_WAGON_CROSSING_TIME 6

/*
	Removes a semaphore from the system.
*/
void unlinkSemaphore(char * semaphoreNameToUnlink) {
	if(sem_unlink(semaphoreNameToUnlink) < 0) {
		if(errno == ENOENT) {
			//perror("This semaphore was already unlinked.\n");
			//don't exit! This isn't a problem!
		} else {
			printf("Error unlinking semaphore!\n");
			exit(EXIT_FAILURE);
		}
	}
}

/*
	Get random number within range of [lowerBound, upperBound]. From StackOverflow.,
	Pre-cond:
		lowerBound < upperBound
*/
int getRandomNumberInRange(int lowerBound, int upperBound) {
	return (rand() % ((upperBound + 1) - lowerBound)) + lowerBound;
}

int main(void) {
	//define rand:
	srand(time(NULL));

	//define semaphore names:
	char * northWaiting_semName = "/northWaiting";
	char * southWaiting_semName = "/southWaiting";
	char * sharedMemoryMutex_semName = "/sharedMemMutex";
	char * mainWaitForWagon_semName = "/mainWaitForWagon";
	char * nonFirstWagonsOnBridge_semName = "/nonFirstWagonsOnBridge";
	char * firstWagonOnBridge_semName = "/firstWagonOnBridge";

	//unlink semaphores from previous iterations to clear left over values
	unlinkSemaphore(northWaiting_semName);
	unlinkSemaphore(southWaiting_semName);
	unlinkSemaphore(sharedMemoryMutex_semName);
	unlinkSemaphore(mainWaitForWagon_semName);
	unlinkSemaphore(nonFirstWagonsOnBridge_semName);
	unlinkSemaphore(firstWagonOnBridge_semName);

	//define shared memory space and shared variables:
	void * sharedMemory = mmap(NULL, SHARED_MEM_SIZE,
		PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, -1, 0);

	int * currentTime = sharedMemory;
	int * minimumTimeWagonCanFinishCrossing = sharedMemory + sizeof(int);
	int * numberOfWagons_northWaitingQueue = minimumTimeWagonCanFinishCrossing + sizeof(int);
	int * numberOfWagons_southWaitingQueue = numberOfWagons_northWaitingQueue + sizeof(int);
	int * currentBridgeDirection = numberOfWagons_southWaitingQueue + sizeof(int);
	int * shouldSwitchCurrentBridgeDirection = currentBridgeDirection + sizeof(int);
	int * numberOfWagons_onBridge = shouldSwitchCurrentBridgeDirection + sizeof(int);
	int * numberWagons_processed = numberOfWagons_onBridge + sizeof(int);

	//initialize shared variables ~
	*currentTime = 0;
	*minimumTimeWagonCanFinishCrossing = 0;
	*numberOfWagons_northWaitingQueue = 0;
	*numberOfWagons_southWaitingQueue = 0;
	*currentBridgeDirection = NORTH;
	*shouldSwitchCurrentBridgeDirection = 0;
	*numberOfWagons_onBridge = 0;

	//link semaphores ~
	//semaphore to hold wagons waiting to cross bridge north:
	sem_t * northWaiting_sem = sem_open(northWaiting_semName, O_CREAT, OPEN_SEM_MODE, 0);
	//semaphore to hold wagons waiting to cross bridge south:
	sem_t * southWaiting_sem = sem_open(southWaiting_semName, O_CREAT, OPEN_SEM_MODE, 0);
	//mutex lock to protect shared memory variables from race conditions:
	sem_t * sharedMemoryMutex_sem = sem_open(sharedMemoryMutex_semName, O_CREAT, OPEN_SEM_MODE, 1);
	//semaphore to prevent main from iterating time until wagons finish processing
	sem_t * mainWaitForWagonToProcess_sem = sem_open(mainWaitForWagon_semName, O_CREAT, OPEN_SEM_MODE, 0);
	//semaphore to store all the non-first wagons on the bridge; we can let one wagon through this semaphore
	sem_t * nonFirstWagonsOnBridge_sem = sem_open(nonFirstWagonsOnBridge_semName, O_CREAT, OPEN_SEM_MODE, 1);
	//semaphore to hold the first wagon on the bridge (since we only need to worry about the first wagon in line):
	sem_t * firstWagonOnBridge_sem = sem_open(firstWagonOnBridge_semName, O_CREAT, OPEN_SEM_MODE, 0);


	//initialize variables for main loop:
	int finishedProcessing = 0;
	int numberOfWagonsSpawned = 0;
	int timeToSpawnNextWagon = 0;
	pid_t newWagon_pid = 0;
	//begin main loop:
	while(!finishedProcessing) {
		//check to see if we're finished processing
		if(numberOfWagonsSpawned == NUM_WAGONS_TO_SPAWN && *numberOfWagons_northWaitingQueue == 0
		&& *numberOfWagons_southWaitingQueue == 0 && *numberOfWagons_onBridge == 0) {
			finishedProcessing = 1;
		}

		/*
			The direction can only flip if a wagon flips it. Thus, if there are no more wagons being spawned, the direction can no longer be flipped.
			So after we let all the wagons from one waitingQueue go and let them process, we should flip the direction and let any wagons in the other
			direction's waiting queue go.
		*/
		if(numberOfWagonsSpawned == NUM_WAGONS_TO_SPAWN && *numberOfWagons_onBridge == 0) {
			if(*currentBridgeDirection == NORTH && *numberOfWagons_northWaitingQueue == 0) {
				*shouldSwitchCurrentBridgeDirection = 1;
			} else if (*currentBridgeDirection == SOUTH && *numberOfWagons_southWaitingQueue == 0) {
				*shouldSwitchCurrentBridgeDirection = 1;
			}
		}

		//switch bridge direction if we should and the bridge is empty:
		if (*shouldSwitchCurrentBridgeDirection == 1 && *numberOfWagons_onBridge == 0) {
			if(*currentBridgeDirection == NORTH) {
				printf("Direction flipped to SOUTH!\n");
				*currentBridgeDirection = SOUTH;
			} else {
				printf("Direction flipped to NORTH!\n");
				*currentBridgeDirection = NORTH;
			}
			*shouldSwitchCurrentBridgeDirection = 0;
		}

		//try to release a wagon from the waiting queues (we shouldn't do this if we're trying to switch direction):
		if(*shouldSwitchCurrentBridgeDirection != 1) {
			//if the bridge direction is NORTH and there are wagons in the north waiting queue, let one go:
			if(*currentBridgeDirection == NORTH && *numberOfWagons_northWaitingQueue != 0) {
				sem_wait(sharedMemoryMutex_sem);
				*numberOfWagons_northWaitingQueue = *numberOfWagons_northWaitingQueue - 1;
				sem_post(sharedMemoryMutex_sem);

				sem_post(northWaiting_sem);
				sem_wait(mainWaitForWagonToProcess_sem);
			} 
			//if the bridge direction is SOUTH and there are wagons in the south waiting queue, let one go:
			else if (*currentBridgeDirection == SOUTH && *numberOfWagons_southWaitingQueue != 0){
				sem_wait(sharedMemoryMutex_sem);
				*numberOfWagons_southWaitingQueue = *numberOfWagons_southWaitingQueue - 1;
				sem_post(sharedMemoryMutex_sem);

				sem_post(southWaiting_sem);
				sem_wait(mainWaitForWagonToProcess_sem);
			}
		}

		//if there is a wagon at the front of the bridge, let it process:
		if(*numberOfWagons_onBridge > 0) {
			sem_post(firstWagonOnBridge_sem);
			sem_wait(mainWaitForWagonToProcess_sem);
		}

		//printf("Current time: %d; timeToSpawnNextWagon: %d\n", *currentTime, timeToSpawnNextWagon);

		//check if we should spawn a new wagon:
		if(timeToSpawnNextWagon <= *currentTime && numberOfWagonsSpawned < NUM_WAGONS_TO_SPAWN) {

			timeToSpawnNextWagon = *currentTime + getRandomNumberInRange(MIN_TIME_TO_SPAWN_NEW_WAGON,
				MAX_TIME_TO_SPAWN_NEW_WAGON);
			numberOfWagonsSpawned++;

			if((newWagon_pid = fork()) < 0) {
				perror("Forking error!");
				exit(EXIT_FAILURE);
			}

			//new wagon processing:
			if(0 == newWagon_pid) {
				//link existing semaphores to this child process:
				northWaiting_sem = sem_open(northWaiting_semName, O_CREAT);
				southWaiting_sem = sem_open(southWaiting_semName, O_CREAT);
				sharedMemoryMutex_sem = sem_open(sharedMemoryMutex_semName, O_CREAT);
				mainWaitForWagonToProcess_sem = sem_open(mainWaitForWagon_semName, O_CREAT);
				nonFirstWagonsOnBridge_sem = sem_open(nonFirstWagonsOnBridge_semName, O_CREAT);
				firstWagonOnBridge_sem = sem_open(firstWagonOnBridge_semName, O_CREAT);

				//set up wagon variables:
				finishedProcessing = 0;
				int timeForWagonToCrossBridge = getRandomNumberInRange(MIN_WAGON_CROSSING_TIME, MAX_WAGON_CROSSING_TIME);
				int timeWagonCanFinishCrossingBridge = 0;
				int wagonDirection = getRandomNumberInRange(0, 10);
				if(wagonDirection > 5) {
					wagonDirection = NORTH;
				} else {
					wagonDirection = SOUTH;
				}

				printf("Wagon %d has arrived! It's going going %s at %d speed\n", numberOfWagonsSpawned,
					wagonDirection == SOUTH ? "SOUTH" : "NORTH", timeForWagonToCrossBridge);

				//if the bridge traffic is going in an opposite direction or we should switch, say we should switch direction:
				if(*currentBridgeDirection != wagonDirection) {
					if(*shouldSwitchCurrentBridgeDirection == 0) {
						sem_wait(sharedMemoryMutex_sem);
						*shouldSwitchCurrentBridgeDirection = 1;
						sem_post(sharedMemoryMutex_sem);
					}
				}

				//if traffic is halted, we should place wagons into the proper waiting queues:
				if(*shouldSwitchCurrentBridgeDirection == 1) {
					//ensure that main can continue since this wagon is done processing for now
					sem_post(mainWaitForWagonToProcess_sem);
					//add this wagon to the proper waiting queue:
					if(wagonDirection == NORTH) {
						sem_wait(sharedMemoryMutex_sem);
						*numberOfWagons_northWaitingQueue = *numberOfWagons_northWaitingQueue + 1;
						sem_post(sharedMemoryMutex_sem);
						printf("Wagon %d has entered NORTH waiting queue\n", numberOfWagonsSpawned);
						sem_wait(northWaiting_sem);
					} else {
						sem_wait(sharedMemoryMutex_sem);
						*numberOfWagons_southWaitingQueue = *numberOfWagons_southWaitingQueue + 1;
						sem_post(sharedMemoryMutex_sem);
						printf("Wagon %d has entered SOUTH waiting queue\n", numberOfWagonsSpawned);
						sem_wait(southWaiting_sem);
					}
					printf("Wagon %d has exited its waiting queue\n", numberOfWagonsSpawned);
				}

				//say that another wagon has gotten on the bridge
				sem_wait(sharedMemoryMutex_sem);
				*numberOfWagons_onBridge = *numberOfWagons_onBridge + 1;
				sem_post(sharedMemoryMutex_sem);

				//since wagon has gotten to the bridge at this point, figure out when it can finish crossing bridge:
				sem_wait(sharedMemoryMutex_sem);
				timeWagonCanFinishCrossingBridge = *currentTime + timeForWagonToCrossBridge;
				//if the wagon wants to finish before the shortest possible time, adjust it so that it finishes as soon as possible:
				if (timeWagonCanFinishCrossingBridge <= *minimumTimeWagonCanFinishCrossing) {
					timeWagonCanFinishCrossingBridge = *minimumTimeWagonCanFinishCrossing + 1;
				}
				*minimumTimeWagonCanFinishCrossing = timeWagonCanFinishCrossingBridge;
				sem_post(sharedMemoryMutex_sem);

				//let main continue since this wagon is done processing and is about to wait into the bridge sem
				sem_post(mainWaitForWagonToProcess_sem);
				//let wagon go into the bridge semaphore:
				printf("Wagon %d has entered the bridge! It will finish at %d\n", numberOfWagonsSpawned, timeWagonCanFinishCrossingBridge);
				sem_wait(nonFirstWagonsOnBridge_sem);

				//once we get here, the wagon is the first wagon in line, so let it go into the first wagon sem:
				printf("Wagon %d has become the first wagon on the bridge! It will finish at %d\n", numberOfWagonsSpawned, timeWagonCanFinishCrossingBridge);
				sem_wait(firstWagonOnBridge_sem);

				//this will iterate while we're in the while loop:
				while(!finishedProcessing) {
					if(timeWagonCanFinishCrossingBridge <= *currentTime) {
						printf("Wagon %d has exited the bridge! It should have finished at %d, and it finished at %d\n", numberOfWagonsSpawned,
							timeWagonCanFinishCrossingBridge, *currentTime);

						sem_wait(sharedMemoryMutex_sem);
						*numberOfWagons_onBridge = *numberOfWagons_onBridge - 1;
						*numberWagons_processed = *numberWagons_processed + 1;
						sem_post(sharedMemoryMutex_sem);

						sem_post(nonFirstWagonsOnBridge_sem);
						sem_post(mainWaitForWagonToProcess_sem);

						finishedProcessing = 1;
						exit(EXIT_SUCCESS);
					}
					sem_post(mainWaitForWagonToProcess_sem);
					sem_wait(firstWagonOnBridge_sem);
				}
			}

			//main processing:
			else {
				sem_wait(mainWaitForWagonToProcess_sem);
			}
		}

		//increment time:
		sem_wait(sharedMemoryMutex_sem);
		*currentTime += 1;
		sem_post(sharedMemoryMutex_sem);
	} //end while loop

	printf("Done processing!! %d wagons were processed, and %d wagons were dispatched\n", *numberWagons_processed, NUM_WAGONS_TO_SPAWN);

	//unmap shared memory space:
	munmap(sharedMemory, SHARED_MEM_SIZE);

	//unlink semaphores:
	unlinkSemaphore(northWaiting_semName);
	unlinkSemaphore(southWaiting_semName);
	unlinkSemaphore(sharedMemoryMutex_semName);
	unlinkSemaphore(mainWaitForWagon_semName);
	unlinkSemaphore(nonFirstWagonsOnBridge_semName);
	unlinkSemaphore(firstWagonOnBridge_semName);
}
