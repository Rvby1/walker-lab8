#include <sys/types.h>     /* for pid_t */
#include <unistd.h>        /* for fork */   
#include <stdio.h>         /* for printf */
#include <sys/wait.h>      /* for waitpid */
#include <stdlib.h>        /* for exit */
#include <semaphore.h>       /* for semaphore processing */
#include <time.h>

#define SEM_PUBLICITY 0 //set to 0 so that semaphores are private to this process
#define SOUTH_DIRECTION 0
#define NORTH_DIRECTION 1

#define MIN_CROSSING_TIME 3 //min time steps it will take a wagon to cross the bridge
#define MAX_CROSSING_TIME 6 //max time steps it will take a wagon to cross the bridge

#define NUMBER_WAGONS_TO_SPAWN 30 

//adapted from geeksforgeeks code: 
int getRandomNumberWithinRange(int lowerBound, int upperBound) { 
	return (rand() % (upperBound - lowerBound)) + lowerBound;
}

int main (void) { 
	srand(time(0));

	int finishedProcessing = 0;
	int numberOfWagonsSpawned = 0;

	//create all of the semaphores for the system:
	//keeps track of the number of time steps
	sem_t time_sem; 
	//keeps track of the current flow of traffic
	sem_t currentDirection_sem; 
	//keeps track of whether or not we should flip direction
	sem_t switchDirection_sem; 
	//holds wagons who are waiting to get on the bridge
	sem_t southWaiting_sem; 
	sem_t northWaiting_sem; 
	//counting semaphore to track the number of wagons currently on the bridge
	sem_t numberOfWagonsOnBridge_sem; 
	//holds an ordered queue of the wagons waiting for a new time step (first wagon on bridge is first in this queue) 
	sem_t wagonsWaitingForTimeStep_sem; 
	//lock to prevent us from executing more than one wagon at a time
	sem_t wagonCurrentlyProcessing_sem; 
	
	//initialize all of the semaphores. All of them start at zero.
	sem_init(&time_sem, SEM_PUBLICITY, 0);
	sem_init(&currentDirection_sem, SEM_PUBLICITY, 0);
	sem_init(&southWaiting_sem, SEM_PUBLICITY, 0);
	sem_init(&northWaiting_sem, SEM_PUBLICITY, 0);
	sem_init(&numberOfWagonsOnBridge_sem, SEM_PUBLICITY, 0);
	sem_init(&wagonsWaitingForTimeStep_sem, SEM_PUBLICITY, 0);	
	sem_init(&wagonCurrentlyProcessing_sem, SEM_PUBLICITY, 0);
	
	while(finishedProcessing != 1) {
		pid_t newWagon_pid;
				
		//to store values of semaphores: 
		int numberOfWagonsOnBridge, 
		currentDirectionValue,
		switchDirectionValue, 
		numberOfWagonsInWaitingQueue;
		
		/*
			Check to see if we have finished processing all the wagons. This will happen 
			when there are no more wagons to spawn and no wagons on the bridge or in the queues. 
		*/
		sem_getvalue(&numberOfWagonsOnBridge_sem, &numberOfWagonsOnBridge);
		if(numberOfWagonsSpawned == NUMBER_WAGONS_TO_SPAWN) { 
			if(numberOfWagonsOnBridge == 0) { 
				sem_getvalue(&northWaiting_sem, &numberOfWagonsInWaitingQueue);
				if(numberOfWagonsInWaitingQueue == 0) { 
					sem_getvalue(&southWaiting_sem, &numberOfWagonsInWaitingQueue);
					if(numberOfWagonsInWaitingQueue == 0) { 
						finishedProcessing = 1;
					}
				}
			}
		}
		
		/*
			Iterate through all of the wagons on the bridge (all of which are waiting for a new time step). 
			Ensure that we only do one at a time to enforce consistent behavior. 
		*/
		sem_getvalue(&numberOfWagonsOnBridge_sem, &numberOfWagonsOnBridge);
		for(int wagonsProcessed = 0; wagonsProcessed < numberOfWagonsOnBridge; wagonsProcessed++) {
			sem_post(&wagonsWaitingForTimeStep_sem);
			//wait a semaphore to stop us from processing two wagons at the same time:
			sem_wait(&wagonCurrentlyProcessing_sem);
		}
		
		/*
			Switch the current direction if there are no wagons on the bridge AND 
			a wagon from the other direction has asked us to switch the direction. 
		*/
		sem_getvalue(&switchDirection_sem, &switchDirectionValue);
		sem_getvalue(&numberOfWagonsOnBridge_sem, &numberOfWagonsOnBridge);
		if(numberOfWagonsOnBridge == 0 && switchDirectionValue == 1) { 
			sem_getvalue(&currentDirection_sem, &currentDirectionValue);
			//signal currentDirection_sem (make it 1) to align with NORTH_DIRECTION: 
			if(currentDirectionValue == SOUTH_DIRECTION) { 
				printf("Direction changed to north.\n");
				sem_post(&currentDirection_sem);
			} 
			//wait currentDirection_sem (make it 0) to align with SOUTH_DIRECTION: 
			else { 
				printf("Direction changed to south.\n");
				sem_wait(&currentDirection_sem);
			}
			sem_wait(&switchDirection_sem);
		}
		
		/*
			If we aren't spawning any more wagons, ensure that we let the wagons from the other waiting queue cross:
		*/
		if(numberOfWagonsSpawned == NUMBER_WAGONS_TO_SPAWN) { 
			if(numberOfWagonsOnBridge == 0) { 
				sem_getvalue(&currentDirection_sem, &currentDirectionValue);
				sem_getvalue(&northWaiting_sem, &numberOfWagonsInWaitingQueue);
				if(currentDirectionValue == NORTH_DIRECTION && numberOfWagonsInWaitingQueue < 0) { 
					sem_post(&currentDirection_sem);
				} 
				
				sem_getvalue(&southWaiting_sem, &numberOfWagonsInWaitingQueue);
				if(currentDirectionValue == SOUTH_DIRECTION && numberOfWagonsInWaitingQueue < 0) { 
					sem_wait(&currentDirection_sem);
				}
			}
		}
		
		/*
			If we're not trying to switch directions AND there is space on the bridge 
			(ie. there are less wagons than the max time it takes a wagon to cross), 
			release a wagon from the waiting list for the current direction.
		*/
		sem_getvalue(&currentDirection_sem, &currentDirectionValue);
		if(switchDirectionValue == 0 && numberOfWagonsOnBridge < MAX_CROSSING_TIME) { 
			if(currentDirectionValue == SOUTH_DIRECTION) { 
				sem_getvalue(&southWaiting_sem, &numberOfWagonsInWaitingQueue);
				if(numberOfWagonsInWaitingQueue < 0) { 
					sem_post(&southWaiting_sem);
				}
			} else { 
				sem_getvalue(&northWaiting_sem, &numberOfWagonsInWaitingQueue);
				if(numberOfWagonsInWaitingQueue < 0) { 
					sem_post(&northWaiting_sem);
				}
			}
		}
		
		//TODO: RANDOMLY DECIDE TIME TO SPAWN NEXT WAGON BETWEEN 1-4
		/*
			Fork a new wagon if we haven't spawned max wagons to spawn.
		*/
		if(numberOfWagonsSpawned == NUMBER_WAGONS_TO_SPAWN) { 
			continue;
		} else { 
			numberOfWagonsSpawned++;
		}
		
		//wait this to prevent us from moving forward until this new wagon has finished processing:
		sem_wait(&wagonCurrentlyProcessing_sem);
		printf("Got before the fork statement\n");
		if((newWagon_pid = fork()) < 0) { 
			perror("Error in creating new wagon process\n");
			exit(EXIT_FAILURE);
		}
		
		//processing for new wagon: 
		if(newWagon_pid == 0) { 
			int wagonCurrentlyProcessingValue;
			
			int positionOnBridge;
			int wagonsOnBridgeDuringLastTimeStep; 
						
			int wagonFinishedProcessing = 0;
			int wagonDirection = getRandomNumberWithinRange(SOUTH_DIRECTION, NORTH_DIRECTION);
			
			int currentTime;
			
			//set the time required for wagon to cross and the time that the wagon can finish:
			int timeRequiredForWagonToCross = getRandomNumberWithinRange(MIN_CROSSING_TIME, MAX_CROSSING_TIME);
			int timeWagonCanFinishCrossing;
			sem_getvalue(&time_sem, &timeWagonCanFinishCrossing);
			timeWagonCanFinishCrossing += timeRequiredForWagonToCross;

			
			printf("Wagon %d arrived at bridge. Heading %d. Will take %d time to cross bridge, so it can finish at %d\n", 
				numberOfWagonsSpawned, wagonDirection, timeRequiredForWagonToCross, timeWagonCanFinishCrossing);
			
			/*
				Check if the wagon is coming from the opposite direction of the current direction. 
				If it is and switchDirection hasn't been flagged yet, flag it. 
			*/
			sem_getvalue(&currentDirection_sem, &currentDirectionValue);
			sem_getvalue(&switchDirection_sem, &switchDirectionValue);
			if(currentDirectionValue != wagonDirection && switchDirectionValue == 0) { 
				sem_post(&switchDirection_sem);
			}
			
			/*
				If we should switch direction (which means that no more wagons should enter the bridge)
				OR the bridge is full, then add the wagon to the proper waiting list
			*/
			sem_getvalue(&switchDirection_sem, &switchDirectionValue);
			sem_getvalue(&numberOfWagonsOnBridge_sem, &numberOfWagonsOnBridge);
			if(numberOfWagonsOnBridge == MAX_CROSSING_TIME || switchDirectionValue == 1) { 
				//let the main process know that this wagon has stopped processing for now
				sem_post(&wagonCurrentlyProcessing_sem); 
				if(currentDirectionValue == SOUTH_DIRECTION) { 
					printf("Wagon %d added to south_waiting\n", numberOfWagonsSpawned);
					sem_post(&southWaiting_sem);
				} else { 
					printf("Wagon %d added to north_waiting\n", numberOfWagonsSpawned);
					sem_post(&northWaiting_sem);
				}
				printf("Wagon %d removed from its waiting queue\n", numberOfWagonsSpawned);
			}
			
			printf("Got before the wagon while loop\n");
			sem_post(&numberOfWagonsOnBridge_sem);
			sem_getvalue(&numberOfWagonsOnBridge_sem, &numberOfWagonsOnBridge);
			wagonsOnBridgeDuringLastTimeStep = numberOfWagonsOnBridge;
			positionOnBridge = numberOfWagonsOnBridge;
			while(wagonFinishedProcessing != 1) { 
				sem_getvalue(&numberOfWagonsOnBridge_sem, &numberOfWagonsOnBridge);
				if(wagonsOnBridgeDuringLastTimeStep > numberOfWagonsOnBridge) { 
					positionOnBridge -= wagonsOnBridgeDuringLastTimeStep - numberOfWagonsOnBridge;
				} 
				
				printf("Got into the wagon while loop\n");

				
				sem_getvalue(&time_sem, &currentTime);
				if(positionOnBridge == 1 && currentTime >= timeWagonCanFinishCrossing) { 
					sem_wait(&numberOfWagonsOnBridge_sem);
					printf("Wagon %d finished crossing the bridge at %d time\n", numberOfWagonsSpawned, currentTime);
					wagonFinishedProcessing = 1;
				}
				
				//wagon 
				sem_post(&wagonCurrentlyProcessing_sem);
				sem_wait(&wagonsWaitingForTimeStep_sem);
			}
		}//end processing for new wagon 
		
		//processing for parent: 
		else { 
			sem_post(&time_sem);
		}
	}//end while
	exit(EXIT_SUCCESS);
}